import { CustomStoryState } from './customStoryState';
import { StoryScene } from './type-story-engine/src/storyScene';

export class CustomStoryScene extends StoryScene {
    state: CustomStoryState;
}