import { InitialScene } from './storyScenes';
import { StoryState } from './type-story-engine/src/storyState';

export class CustomStoryState extends StoryState {
    name: string;

    constructor() {
        super(InitialScene);
    }
}