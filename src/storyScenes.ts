import { CustomStoryScene } from './customStoryScene';
import { StoryChoice } from './type-story-engine/src/storyElement';

export class InitialScene extends CustomStoryScene {
    render() {
        return [`This is a demo project for type-story-engine.
            Type "help" to view list of available commands`,

            `Scenes are the basic building blocks of the interactive story
            Each scene's class name must be unique
            Each scene has two methods: render() and receiveCommand()`
        ];
    }

    receiveCommand() {
        return ChooseNameScene;
    }
}

export class ChooseNameScene extends CustomStoryScene {
    render() {
        return [`Please type in your name. Type empty string to choose the default name: Meow`];
    }

    receiveCommand(name: string) {
        if (name == '')
            name = 'Meow';
        this.state.name = name;
        return AwakeningScene;
    }
}

export class AwakeningScene extends CustomStoryScene {
    render() {
        return [`One night you wake up in your bed with a start. It is dark. You feel like somebody is watching you. You shift your gaze from the ceiling to your right and see...`,
            new StoryChoice('1', 'A cat'),
            new StoryChoice('2', 'A fox'),
            new StoryChoice('0', 'No one'),
        ];
    }

    receiveCommand(choiceId: string) {
        switch (choiceId) {
            case '1': return CatScene;
            case '2': return FoxScene;
            case '0': return BoringScene;
        }
    }
}

export class CatScene extends CustomStoryScene {
    render() {
        return [`The cat stares back at you.`,
            `Cat: feed me, ${this.state.name}!`,
            `You pour some food into the bowl and go back to sleep`,
            `GOOD ENDING`];
    }
}

export class FoxScene extends CustomStoryScene {
    render() {
        return [`A fox stares back at you.`,
            `Fox: Hello there, ${this.state.name}. I am a sly kitsune. I came to devour your soul.`,
            `The kitsune sucks your soul our of you.`,
            `BAD ENDING`];
    }
}

export class BoringScene extends CustomStoryScene {
    render() {
        return [`${this.state.name} sees no one and goes back to sleep.`,
            `BORING ENDING`];
    }
}