import { StoryOptions } from './type-story-engine/src/storyOptions';
import * as storyScenes from './storyScenes'
import { buildStorySceneMap } from './type-story-engine/src/storyScene';
import { CustomStoryState } from './customStoryState';

export const storyOptions = new StoryOptions();
storyOptions.appName = 'weird-dream-story';
storyOptions.sceneMap = buildStorySceneMap([storyScenes]);
storyOptions.stateConstructor = CustomStoryState;
