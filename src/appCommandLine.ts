import 'source-map-support/register';
import { storyOptions } from './appBase';
import { CommandLineStoryRunner } from './type-story-engine/src/commandLineStoryRunner';

class AppCommandLine {
    async run() {
        const runner = new CommandLineStoryRunner(storyOptions);
        await runner.run();
    }
}

new AppCommandLine().run();