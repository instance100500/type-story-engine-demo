# Type Story Engine Demo

Introduction to type-story-engine

## Getting started

To use the engine in the project, add it to your project as git submodule:
`git submodule add https://gitlab.com/instance100500/type-story-engine.git ./src/type-story-engine`

Why use submodule instead of npm package:
* Full source is available: read, navigate
* Full stack traces with line numbers are available

## Project structure
* `appBase.ts`: defines basic options for the whole project
    * `appName`: defines package name
    * `sceneMap`: defines all scenes in the project
        * Scenes can be grouped in source files. For example, by story chapter:
            * Chapter 1: 10 classes
            * Chapter 2: 12 more classes
            * ... and so on
        * `buildStorySceneMap` function should be called with all source files listed as parameters: `buildStorySceneMap([chapter1, chapter2, andSoOn]);`
    * `stateConstructor`: defines constructor for state
* `appCommandLine.ts`: Create command line runner and start the story.
    * This is the entry point file, so it can be used to run story in command-line mode:
        * `node ./compiled/appCommandLine.js`
* `customStoryScene.ts`: Here we define base class for all story scenes in our story.
    * One thing that should be redefined here is the type of the `state` field to let us use our custom state
* `customStoryState.ts`: Here we define state for our story
    * Custom state may contain any number of fields:
        * Name of the character
        * Stats: strength, speed, agility and so on
        * Inventory: weapons, clothing
        * Decisions made in the past
            * There is currently no plan to discard information about past choices. In a long story, each decision will be always stored, even if it was made in the very beginning and is no longer relevant. This might be a problem for a very long story with lots of choices because the State object will keep getting bigger.
    * State constructor: should provide initial scene of the story
    * The story engine will store state object as a string, serialized to JSON
* `storyScenes.ts`: define story scenes. All story scene classes should be `export` -ed

## Scene structure

Each scene should inherit `CustomStoryScene` which is in turn inherited from `StoryScene`.
Each scene should have two methods: `render()` and `receiveCommand()`.

### render()
Return representation of the current scene for the user. The method should return array of `StoryElement`. See definition of `StoryElement`: it is either a `string` or an object of type `StoryChoice`. The simplest form of `render()` function is: returning a text

```ts
    render() {
        return ['Something happens'];
    }
```

It is **not** allowed to modify `this.state` from `render()`.

### receiveCommand()
Receive input from the user as a string and return next scene's constructor. The user's input does not necessarily need to be parsed. Linear scenes might simply return the next scene's constructor.

Example of a simple receiveCommand() function:
```ts
    receiveCommand() {
        return ChooseNameScene;
    }
```

Example of a choosy receiveCommand() function
```ts
    receiveCommand(choiceId: string) {
        switch (choiceId) {
            case '1': return CatScene;
            case '2': return FoxScene;
            case '0': return BoringScene;
        }
    }
```

In the example above, if the user's input does not match any of the cases (1, 2, 0) then the return value of the function will be `undefined`, and the same scene will be shown to the user again.

Function `receiveCommand()` can make any number of changes to `this.state`.